# Wordle

This is a project for wordle game.


You have to guess a five-letter word and you have to keep attempting till 6 chances till you get it right or you fail.

If it turns green, that letter is in the word and you've placed it in the right spot.
If it turns yellow, the letter is in the word but you have it in the wrong position.
If the box turns red, it means the letter isn't in the word at all.



