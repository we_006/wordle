import requests as rq
import random

DEBUG = False


class WordleBot:
	words = [word.strip() for word in open("5letters.txt")]
	mm_url = "https://we6.talentsprint.com/wordle/game/"
	register_url = mm_url + "register"
	creat_url = mm_url + "create"
	guess_url = mm_url + "guess"

	def __init__(self, name: str):
		def is_unique(w: str) -> bool:
			return len(w) == len(set(w))
		self.session = rq.session()
		register_dict = {"mode": "wordle", "name": name}
		reg_resp = self.session.post(WordleBot.register_url, json=register_dict)
		self.me = reg_resp.json()['id']
		creat_dict = {"id": self.me, "overwrite": True}
		self.session.post(WordleBot.creat_url, json=creat_dict)
		self.choices = [w for w in WordleBot.words[:] if is_unique(w)]
		random.shuffle(self.choices)
	def play(self) -> str:
		def post(choice: str) -> tuple[int, bool]:
			guess = {"id": self.me, "guess": choice}
			response = self.session.post(WordleBot.guess_url, json=guess)
			rj = response.json()
			right = (rj["feedback"])
			status = "win" in rj["message"]
			return right, status
		choice = random.choice(self.choices)
		self.choices.remove(choice)
		right, won = post(choice)
		tries = [f'{choice}:{right}']
		for i in range(6):
			while not "win":
				#if DEBUG:
					#print(choice, right, self.choices[:10])
				self.update(choice, right)
				choice = random.choice(self.choices)
				self.choices.remove(choice)
				right, won = post(choice)
				tries.append(f'{choice}:{right}')
		print("Secret is", choice, "found in", len(tries), "attempts")
		print("Route is:", " => ".join(tries))
	def update(self, choice: str, right:str):
		def common(choice: str, word: str):
			return len(set(choice) & set(word))
		self.choices = [w for w in self.choices if common(choice, w) == right]


game = WordleBot("CodeShifu")
game.play()

