import requests as rq
from typing import Self
import random

DEBUG = False

<<<<<<< HEAD
class MMBot:
    words = [word.strip() for word in open("words.txt")]
=======
class WordleBot:
    
    with open("5letters.txt") as f:
        words = [word.strip() for word in f.readlines()]
>>>>>>> 6ebb1dc136a69ff648507bf84481e40917ca0b88
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"

<<<<<<< HEAD
    def __init__(self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))
=======
    def __init__(self: Self, name: str):
        def is_unique(w: str) -> bool:
            return len(w) == len(set(w))

>>>>>>> 6ebb1dc136a69ff648507bf84481e40917ca0b88
        self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id": self.me, "overwrite": True}
<<<<<<< HEAD
        self.session.post(MMBot.creat_url, json=creat_dict)
        self.choices = [w for w in MMBot.words[:] if is_unique(w)]
=======
        self.session.post(WordleBot.creat_url, json=creat_dict)

        self.choices = [w for w in WordleBot.words if is_unique(w)]
>>>>>>> 6ebb1dc136a69ff648507bf84481e40917ca0b88
        random.shuffle(self.choices)

    def play(self: Self) -> str:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
<<<<<<< HEAD
            right = rj["feedback"]
=======
            feedback = rj["feedback"]
>>>>>>> 6ebb1dc136a69ff648507bf84481e40917ca0b88
            status = "win" in rj["message"]
            return feedback, status

        tries = []
        for _ in range(6):
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            feedback, won = post(choice)
            tries.append(f'{choice}:{feedback}')

            if DEBUG:
                print(choice, feedback, self.choices[:10])

            if won or feedback == 'ggggg':
                print("Secret is", choice, "found in", len(tries), "attempts")
                print("Route is:", " => ".join(tries))
                print("You won!")
                return

            self.update(choice, feedback)

        print("You lose!")
        print("Route is:", " => ".join(tries))

    def update(self: Self, choice: str, feedback: str):
        def match_feedback(word: str) -> bool:
            for i in range(5):
                if feedback[i] == 'g' and word[i] != choice[i]:
                    return False
                if feedback[i] == 'y' and (word[i] == choice[i] or choice[i] not in word):
                    return False
                if feedback[i] == 'r' and choice[i] in word:
                    return False
            return True

<<<<<<< HEAD
game = MMBot("CodeShifu")
=======
        self.choices = [w for w in self.choices if match_feedback(w)]


game = WordleBot("CodeShifu")
>>>>>>> 6ebb1dc136a69ff648507bf84481e40917ca0b88
game.play()
